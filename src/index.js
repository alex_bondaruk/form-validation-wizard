import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Values} from 'redux-form-website-template';
import store from './store';
import showResults from './showResults';
import WizardForm from './WizardForm';
import index from './index.css';


const rootEl = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
    <div className="wrapper">
      <div className="form__block">
        <WizardForm onSubmit={showResults}/>
      </div>
    </div>
  </Provider>,
  rootEl,
);
