import React from 'react';
import {reduxForm } from 'redux-form';



const WizardFormThirdPage = props => {
  const { handleSubmit} = props;
  return (
    <form onSubmit={handleSubmit}>
      <div className="ok"></div>
      <button type="submit" className="next next_finish">Go to Dashboard<i class="arrow right"></i></button>
    </form>
  );
};
export default reduxForm({
  form: 'wizard', //                 <------ same form name
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
})(WizardFormThirdPage);
