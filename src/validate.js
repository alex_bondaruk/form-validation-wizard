const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  if (!values.password) {
    errors.password = 'Required'
  }else if (values.password.length < 6) {
    errors.password = 'Password should be < 6 symbols'
  }
  if (!values.confirm_password) {
    errors.confirm_password = 'Required'
  }
  if (values.password!==values.confirm_password) {
    errors.confirm_password = 'Passwords not the same'
  }
  if (!values.day) {
    errors.day = 'Required';
  }
  if (!values.day) {
    errors.day = 'Required';
  }
  if (!values.month) {
    errors.month = 'Required';
  }
 let today = new Date();
  if (!values.year) {
    errors.year = 'Required';
  }
  if (today.getFullYear() - values.year < 18) {
      errors.year = 'Required';
  }
  if (!values.sex) {
    errors.sex = 'Required';
  }
  if (!values.hear) {
    errors.hear = 'Required';
  }
  return errors;
};

export default validate;
