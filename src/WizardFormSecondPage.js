import React from 'react';
import { Field, reduxForm } from 'redux-form';
import validate from './validate';
import renderField from './renderField';

const renderError = ({ meta: { touched, error } }) =>
  touched && error ? <span>{error}</span> : false;

const hear = ['Social sites', 'Friends', 'Advertising'];
const renderHear = ({ input, meta: { touched, error } }) => (
  <div>
    <select {...input}>
      <option value="" disabled selected></option>
      {hear.map(val => <option value={val} key={val}>{val}</option>)}
    </select>
    {touched && error && <span>{error}</span>}
  </div>
);

const day = Array.from(new Array(31), (x,i) => i + 1);
const Day = ({ input, meta: { touched, error } }) => (
  <div className="date__input">
    <select {...input}>
      <option value="" disabled selected>DD</option>
      {day.map(val => <option value={val} key={val}>{val}</option>)}
    </select>
    {touched && error && <span>{error}</span>}
  </div>
);

const month = Array.from(new Array(12), (x,i) => i + 1);
const Month = ({ input, meta: { touched, error } }) => (
  <div className="date__input">
    <select {...input}>
      <option value="" disabled selected>MM</option>
      {month.map(val => <option value={val} key={val}>{val}</option>)}
    </select>
    {touched && error && <span>{error}</span>}
  </div>
);

const year = Array.from(new Array(100), (x,i) => i + 1917);
const Year = ({ input, meta: { touched, error } }) => (
  <div className="date__input">
    <select {...input}>
      <option value="" disabled selected>YYYY</option>
      {year.map(val => <option value={val} key={val}>{val}</option>)}
    </select>
    {touched && error && <span>{error}</span>}
  </div>
);

const WizardFormSecondPage = props => {
  const { handleSubmit, previousPage } = props;
  return (
    <form onSubmit={handleSubmit}>
      <div className="date_wrapper">
        <label>Date of birth</label>
        <Field name="day" className="date" component={Day} />
        <Field name="month" className="date" component={Month} />
        <Field name="year" className="date" component={Year} />
      </div>
      <div className="gender">
        <p>Gender</p>
          <label className="male">
            <Field name="sex" component="input" type="radio" value="male" />
            {' '}
            <span>Male</span>
          </label>
          <label className="female">
            <Field name="sex" component="input" type="radio" value="female" />
            {' '}
            <span>Female</span>
          </label>
          <label className="unspecified">
            <Field name="sex" component="input" type="radio" value="unspecified" />
            {' '}
            <span>Unspecified</span>
          </label>
          <Field name="sex" component={renderError} />
        </div>
      <div className="hear__wrapper">
        <label>Where do you hear about us?</label>
        <Field name="hear" component={renderHear} />
      </div>
      <div className="hr"></div>
      <div className="click">
        <button type="button" className="previous" onClick={previousPage}>
          Back
        </button>
        <button type="submit" className="next">Next<i class="arrow right"></i></button>
      </div>
    </form>
  );
};

export default reduxForm({
  form: 'wizard', //                 <------ same form name
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate,
})(WizardFormSecondPage);
