import React from 'react'

const renderField = ({input, label, placeholder, type, meta: {touched, error}}) => (
  <div>
    {touched && error ? <label className="red">{label}</label> : <label>{label}</label>}
    <div>
      <input {...input} type={type} />
    </div>
  </div>
)
export default renderField